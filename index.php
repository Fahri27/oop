<?php

/* require('animal.php'); */
require('frog.php');
require('ape.php');

$sheep = new animal("shaun");
echo "Name : $sheep->name <br>";
echo "legs : $sheep->legs <br>";
echo "cold blooded : $sheep->cold_blooded <br> <br>";

$kodok = new frog("buduk");
echo "Name : $kodok->name <br>";
echo "legs : $kodok->legs <br>";
echo "cold blooded : $kodok->cold_blooded <br>";
echo ": hop-hop" . $kodok->jump();
echo "<br> <br>";

$monyet = new ape("kera sakti");
echo "Name : $monyet->name <br>";
echo "legs : $monyet->legs <br>";
echo "cold blooded : $monyet->cold_blooded <br>";
echo ": Auoooo" . $monyet->yell();